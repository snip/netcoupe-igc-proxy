# netcoupe-igc-proxy

Proxy-cache pour les fichiers IGC de la Netcoupe.

Ce script joue le rôle de proxy-cache pour les fichiers IGC de la [netcoupe](http://netcoupe.net).
Il permet de servir les fichiers IGC en les récupérant de la netcoupe (proxy) et les enregistre en local pour les fois d'après (cache).

Mis en place initialement pour pour contourner le fait que les navigateurs récents ne permettent pas de charger du contenu depuis un site HTTP (netcoupe) si le site d'origine est en HTTPS (cartes.1222.fr).

## Dépendances
* PHP

## Installation
```
mkdir -p igc/20{00..99}
chown -R www-data:www-data igc
```

## URL rewriting avec Apache2
Voir le fichier `.htaccess`


## Utilisation
Pour l'année actuelle :
* https://cartes.1222.fr/netcoupe/netcoupe.php?FileID=742
* https://cartes.1222.fr/netcoupe/742
* https://cartes.1222.fr/netcoupe/2019/742

Pour les années d'avant :
* https://cartes.1222.fr/netcoupe/netcoupe.php?year=2018&FileID=33993
* https://cartes.1222.fr/netcoupe/2018/33993
