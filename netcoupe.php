<?php
function getUrl($year, $fileid) {
  $this_year = intval(date('Y'));

  if ($year <= 2017) {
    $url = 'http://archive' . $year . '.netcoupe.net/Download/DownloadIGC.aspx?FileID=' . $fileid;
  } else if ($year < $this_year) {
    $url = 'http://archive' . $year . '.netcoupe.net/Download/DownloadIGC_QFU.aspx?FileID=' . $fileid;
  } else {
    //$url = 'http://netcoupe.net/Download/DownloadIGC.aspx?FileID=' . $fileid;
    $url = 'http://prod.netcoupe.net/Download/DownloadIGC_QFU.aspx?FileID=' . $fileid;
  }

  return $url;
}

$this_year = intval(date('Y'));
$year = !empty($_GET['year']) ? intval($_GET['year']) : $this_year;
if ($year < 2016 || $year > $this_year) {
  $year = $this_year;
}

$fileid = $_GET['FileID'];

$local_file = dirname(__FILE__) . '/igc/' . $year . '/' . $fileid . '.igc';

// Si le fichier n'est pas en local, le récupérer depuis la netcoupe
if (!file_exists($local_file)) {
  $url = getUrl($year, $fileid);
  $igc_file = file_get_contents($url);
  file_put_contents($local_file, $igc_file);
}

// Fournir le fichier
$igc_file = file_get_contents($local_file);
header('Content-Type: text/plain');
header("Content-disposition: attachment; filename=\"" . basename($local_file) . "\""); 
print($igc_file);


